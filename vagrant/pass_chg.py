import pexpect
import time
import sys

def change_password(my_user,my_pass):

    passwd = pexpect.spawn("/usr/bin/passwd {0}".format(my_user) )
    passwd.logfile = sys.stdout
    passwd.expect("New password: ",timeout=3000)
    passwd.sendline(my_pass)
    passwd.expect("Retype new password: ",timeout=3000)
    passwd.sendline(my_pass)
    passwd.expect("successfully",timeout=3000)
    print passwd.before

def main():
    change_password("root", "WTmfs4us!")

if __name__ == '__main__':
    main()



