import pexpect
import time
import os
import sys

def set_key(my_user,my_pass,filename="00_list"):
    with open(filename) as f:
      for line in f:
        pex = pexpect.spawn("sed -i '/{0}/d' {1}/.ssh/known_hosts".format(line.rstrip(),os.environ['HOME']))
        pex.logfile = sys.stdout
        pex.expect(pexpect.EOF,timeout=3000)
        pex = pexpect.spawn("ssh-copy-id {0}@{1}".format(my_user,line.rstrip()))
        pex.logfile = sys.stdout
        pex.expect("Are you sure you want to continue connecting \(yes/no\)\? ",timeout=5000)
        pex.sendline("yes")
        resultid=pex.expect([" password: ","to make sure we haven't added extra keys that you weren't expecting."],timeout=5000)
        if resultid==0:
          pex.sendline(my_pass)
        elif resultid==1:
          pex.expect(pexpect.EOF,timeout=3000)
        else:
          return
        pex.expect(pexpect.EOF,timeout=3000)

def main():
    set_key(my_user="root",my_pass="WTmfs4us!")
    #set_key(my_user="imail",my_pass="imail",filename="00_listimail")

if __name__ == '__main__':
    main()


