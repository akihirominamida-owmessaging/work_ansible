# 使いかた

## 以下を更新する

* 00_list sshkeyを配るホストを指定
* hosts ansibleから接続できるipを書く
* hosts_org 試験環境のホスト上のhostsファイル

## keyを配る

```python 00_ssh.py```

## ansible実行

```python 01_do_ansib```

# Known issue

たまにscalityの設定がうまくいかない

connecterがringに追加されておらず、S3ユーザが登録されていないなど。


# TODO

## 追加する

* nginx
* mxos
* affinitymanager
* scality-index
* scality-proxy

## 改造する

IPなどを手で書き換える必要がある。
Webなどに情報を集約できないか？
