#!/usr/bin/env python

import sys
import time
import pexpect

cfg_filename='/tmp/vi_scality'

cmd='su - imail'
prompt='[#\$%] \Z'


c=pexpect.spawn(cmd,timeout=100000)
c.logfile=file('zzzz','w')

c.expect(prompt)
sys.stdout.write(c.before+c.match.group(0))

c.sendline('unalias ls')
c.expect(prompt)
sys.stdout.write(c.before+c.match.group(0))

c.sendline('$INTERMAIL/lib/imservctrl start imconfserv')
c.expect(prompt)

c.sendline('imconfedit')
c.expect('(\"\S+config.db.edit.\d+\"\s+\d+L,\s+\d+C)')
sys.stdout.write(c.before+c.match.group(1))
time.sleep(1)
c.send(':')
time.sleep(1)
c.send('%s/mssClusterX/mssclusterx/g\r')
time.sleep(3)
c.send(':')
time.sleep(1)
c.send('$r {0}\r'.format(cfg_filename))
time.sleep(3)
c.send(':')
time.sleep(1)
c.send('wq!\r')
while True:
    i=c.expect(['Do you want to install the changes now \([^\)]+\) \[[^\]]+\] \?','Do you want to assess the file even though it\'s unchanged \([^\)]+\) \[[^\]]+\] \?','Do you want to assess the changes now \([^\)]+\)\s+\[[^\]]+\]\s+\?','--More--','Hit <RETURN> to continue ...',prompt])
    if i == 0 or i == 1 or i==2:
        sys.stdout.write(c.before+c.match.group(0))
        c.send('p\r\n')
    elif i == 3 or i == 4:
        sys.stdout.write(c.before+c.match.group(0))
        c.send('\r\n')
    else:
        sys.stdout.write(c.before+c.match.group(0))
        break
c.sendline('exit')
c.expect(pexpect.EOF)
sys.stdout.write(c.before)
c.close()

