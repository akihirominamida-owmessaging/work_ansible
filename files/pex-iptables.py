import time
import sys
import pexpect

def iptabs_config():

    rc = pexpect.spawn("/root/iptables.sh")
    rc.logfile = sys.stdout
    rc.expect("Ctrl-C to finish.",timeout=3000)
    rc.sendcontrol('c')
    rc.expect(pexpect.EOF)

def main():
    iptabs_config()

if __name__ == '__main__':
    main()


