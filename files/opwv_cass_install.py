#!/usr/bin/env python

import datetime
import sys
import os
import logging
import optparse
import pexpect

date_str=datetime.datetime.today().strftime("%Y%m%d%H%M%S")

enter_dict={}
enter_dict['JAVA_HOME']='/opt/jdk'
enter_dict['Would you like to perform a single node installation?']='yes'
enter_dict['What is the node type of this Openwave MSS/QueueServer Cassandra Database?']='1'

dup_questions={}
dup_questions['Do you accept these settings?']=[
    ['location',''],
    ['server',''],
    ['mta',''],
    ['queue',''],
    ['pop',''],
    ['imap',''],
    ['mss',''],
    ['account',''],
    ['schema1','no'],
    ['schema2','']
]


if not os.path.isdir('./log'):  os.makedirs('./log')

opelog_file='log/{0}.{1}.trace'.format(sys.argv[0],date_str)
log_file='log/{0}.{1}.log'.format(sys.argv[0],date_str)

my_format=logging.Formatter('%(asctime)s %(levelname)-8s %(processName)-8s %(threadName)-10s %(message)s')
my_baselogger=logging.getLogger()

console_log = logging.StreamHandler()
file_log = logging.FileHandler(filename=log_file)
my_baselogger.addHandler(console_log)
my_baselogger.addHandler(file_log)
my_baselogger.setLevel(logging.DEBUG)

opt = optparse.OptionParser()
opt.add_option("-d", action='store_const', const=logging.DEBUG, default=logging.INFO, dest='loglevel')
opt.add_option("-t", type=int, action='store', default=3000, dest='timeout')
opt.add_option("-i", default='/root/OPWVCassandra_Package', dest='installer_path')
(options, args) = opt.parse_args()

console_log.setLevel(options.loglevel)
console_log.setFormatter(my_format)
file_log.setLevel(options.loglevel)
file_log.setFormatter(my_format)

my_baselogger.debug('args={0},options={1}'.format(args,options))


cmd='su - root'
prompt='[#\$%] \Z'


c=pexpect.spawn(cmd,timeout=options.timeout)
c.logfile = file(opelog_file,'w')
c.expect(prompt)

c.sendline('unalias ls')
c.expect(prompt)

c.sendline('cd '+options.installer_path)
c.expect(prompt)

c.sendline('pwd')
c.expect(options.installer_path)
c.expect(prompt)

c.sendline('ls -l')
c.expect(prompt)

c.sendline('./Install.sh OPWVCassandra')
c.expect('Are you ready to proceed\? \(yes/quit\) \[[^\]]*\]: ')
c.sendline('')

while True:
    i = c.expect(['(?P<body>[^\?\r\n]+\?) *(?P<option>(\([^\)\r\n]+\)){0,1}) *(?P<default>(\[[^\r\n\]]+\]){0,1}) \Z','(?P<body>  +[^\r\n]+ +:) +(?P<default>\[[^\r\n\]]+\]) \Z', '(?P<body>[^\s\r\n]+)\s+:\s+\Z' ,prompt,'(?P<output>[^\r\n]+)[\r\n]+',pexpect.EOF])
    if i == 0 :
        my_baselogger.debug( '---' )
        my_baselogger.debug( c.match.group('body') )
        my_baselogger.debug( c.match.group('option') )
        my_baselogger.debug( c.match.group('default') )
        if c.match.group('body') in enter_dict:
            my_baselogger.debug( 'enter>>'+enter_dict[c.match.group('body')])
            c.sendline(enter_dict[c.match.group('body')])
        elif c.match.group('body') in dup_questions.keys():
            tmp_cmd=dup_questions[c.match.group('body')].pop(0)
            my_baselogger.debug( 'enter>>'+tmp_cmd[1]+'<< for '+tmp_cmd[0])
            c.sendline(tmp_cmd[1])
        else:
            my_baselogger.debug( 'enter>><<')
            c.sendline('')
    elif i == 1:
        my_baselogger.debug( '---' )
        my_baselogger.debug( c.match.group('body') )
        my_baselogger.debug( c.match.group('default') )
        if c.match.group('body') in enter_dict:
            my_baselogger.debug( 'enter>>'+enter_dict[c.match.group('body')])
            c.sendline(enter_dict[c.match.group('body')])
        elif c.match.group('body') in dup_questions.keys():
            tmp_cmd=dup_questions[c.match.group('body')].pop(0)
            my_baselogger.debug( 'enter>>'+tmp_cmd[1]+'<< for '+tmp_cmd[0])
            c.sendline(tmp_cmd[1])
        else:
            my_baselogger.debug( 'enter>><<')
            c.sendline('')
    elif i == 2:
        my_baselogger.debug( '---' )
        my_baselogger.debug( c.match.group('body') )
        if c.match.group('body') in enter_dict:
            my_baselogger.debug( 'enter>>'+enter_dict[c.match.group('body')])
            c.sendline(enter_dict[c.match.group('body')])
        elif c.match.group('body') in dup_questions.keys():
            tmp_cmd=dup_questions[c.match.group('body')].pop(0)
            my_baselogger.debug( 'enter>>'+tmp_cmd[1]+'<< for '+tmp_cmd[0])
            c.sendline(tmp_cmd[1])
        else:
            my_baselogger.debug( 'enter>><<')
            c.sendline('')
    elif i == 4:
        my_baselogger.debug( 'output>'+c.match.group('output') )
    elif i == 3:
        my_baselogger.debug( '--- got prompt' )
        my_baselogger.debug( 'END' )
        break


c.sendline('exit')
c.expect(pexpect.EOF)
c.close()

