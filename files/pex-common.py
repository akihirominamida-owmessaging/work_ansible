import time
import sys
import logging
import json
import pexpect
import myutils

def interact_cmd(cmd='',interact={},timeout=3000):

    rc = pexpect.spawn(cmd)
    rc.logfile = sys.stdout
    q_list = interact.keys()
    a_list = [interact[key] for key in q_list]
    q_list.extend([pexpect.EOF])
    while True:
        matched = rc.expect(q_list,timeout=timeout)
        if matched == len(a_list) :  # EOF
            break
        rc.send(a_list[matched])

def parsed_args():
    import argparse
    from logging import DEBUG,INFO

    parser = argparse.ArgumentParser()
    parser.add_argument('-D', '--Debug', action='store_const', const=DEBUG, default=INFO, dest='loglevel')
    parser.add_argument('-c', '--command')
    parser.add_argument('-t', '--timeout', default=3000, type=int)
    parser.add_argument('expect', nargs='*', default='{"Enter password: ":"\\r"}', help='')

    result =  parser.parse_args()
    return result

def main():
    args=parsed_args()

    myutils.set_logger(__name__).setLevel(args.loglevel)
    logger=logging.getLogger(__name__)
    logger.debug("args={0}".format(args))

    interact_spec=json.loads(args.expect)

    interact_cmd(cmd=args.command,interact=interact_spec,timeout=args.timeout)

if __name__ == '__main__':
    main()


