import time
import sys
import pexpect

def spv_config():

    rc = pexpect.spawn("/usr/local/bin/scality-supervisor-config")
    rc.logfile = sys.stdout
    rc.expect("Do you accept the licence \? ",timeout=3000)
    rc.sendline("yes")
    i = rc.expect(["keep old config \(y/n\) \? \[y\] ", "install new config"],timeout=3000)
    if i == 0:
      rc.sendline("y")
    elif i==1:
      pass
    rc.expect(pexpect.EOF)

def main():
    spv_config()

if __name__ == '__main__':
    main()


