#!/usr/bin/env python

import sys
import time
import pexpect

cmd='su - imail'
prompt='[#\$%] \Z'


c=pexpect.spawn(cmd,timeout=100000)
c.logfile=file('zzzz','w')

c.expect(prompt)
sys.stdout.write(c.before+c.match.group(0))

c.sendline('unalias ls')
c.expect(prompt)
sys.stdout.write(c.before+c.match.group(0))

c.sendline('imconfedit')
c.expect('(\"\S+config.db.edit.\d+\"\s+\d+L,\s+\d+C)')
sys.stdout.write(c.before+c.match.group(1))
time.sleep(1)
c.send(':')
time.sleep(1)
c.send('%s/\[sn eq\]/')
c.send(' \[sn eq,reverse,sub\]')
c.sendcontrol('v');
c.sendcontrol('m')
c.send(' \[displayname eq,reverse,sub\]')
c.sendcontrol('v');
c.sendcontrol('m')
c.send(' \[givenname eq,reverse,sub\]')
c.sendcontrol('v');
c.sendcontrol('m')
c.send(' \[corpmailprimaryaddress eq,reverse,sub\]')
c.send('/g\r')
time.sleep(1)
c.send(':')
time.sleep(1)
c.send('wq!\r')
while True:
    i=c.expect(['Do you want to install the changes now \([^\)]+\) \[[^\]]+\] \?','Do you want to assess the file even though it\'s unchanged \([^\)]+\) \[[^\]]+\] \?','Do you want to assess the changes now \([^\)]+\)\s+\[[^\]]+\]\s+\?','Hit <RETURN> to continue ...',prompt])
    if i == 0 or i == 1 or i==2:
        sys.stdout.write(c.before+c.match.group(0))
        c.send('p\r\n')
    elif i == 3:
        sys.stdout.write(c.before+c.match.group(0))
        c.send('\r\n')
    else:
        sys.stdout.write(c.before+c.match.group(0))
        break
c.sendline('exit')
c.expect(pexpect.EOF)
sys.stdout.write(c.before)
c.close()

