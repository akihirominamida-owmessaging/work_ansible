import time
import sys
import pexpect

def node_config():

    rc = pexpect.spawn("./scality-node-config --prefix /mnt/disk --disks 3 --nodes 6")
    rc.logfile = sys.stdout
    i = rc.expect(["keep old config \[y\]: ", "install new config"],timeout=3000)
    if i == 0:
      rc.sendline("y")
    elif i==1:
      pass
    rc.expect(pexpect.EOF)

def main():
    node_config()

if __name__ == '__main__':
    main()


