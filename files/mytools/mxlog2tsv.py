#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import datetime
import time
import logging
import optparse

def open_file(file=None, flag="r",buf=100):
    logging.debug('[open_file]')
    if file is not None :
        try:
            return open(file,flag,buf)
        except:
            logging.warning("Fail to open file %s. Openning sys.stdin instead of.", file)
    if flag == "r":
        return sys.stdin
    elif flag == "rw":
        return sys.stdout

def print_summary(printts,summary,count):
    print datetime.datetime.fromtimestamp(printts).strftime('%Y/%m/%d %H:%M:%S'),
    for tmp in summary:
        print ",%d"%count[tmp],
        count[tmp]=0
    print ""


class Summary(object):
    def __init__(self):
        self.data={}

    def push(self,printts,summary,count):
        self.data[printts]={}
        for tmp in summary:
            self.data[printts].update({ tmp:count[tmp] })
            count[tmp]=0

    def output(self,summary):
        for tmp in sorted(summary):
            print ",%s"%tmp,
        print
        for ts in sorted(self.data.keys()):
            print "%s"%datetime.datetime.fromtimestamp(ts).strftime('%Y/%m/%d %H:%M:%S'),
            for tmp in sorted(summary):
                if self.data[ts].has_key(tmp):
                    print ",%d"%self.data[ts][tmp],
                else:
                    print ",%d"%0,
            print

def main():

    regx_mxlog=re.compile(r'^(?P<time>\d{4}\d{2}\d{2} \d{2}\d{2}\d{2}\d{3}[\+\-]\d{4}) (?P<mx_host>\S+) (?P<mx_module>\S+) (?P<mx_pid>\d+) (\d+) (\d+) (?P<mx_level>[^;]+);(?P<mx_event>[^: \r\n]+)(?P<mx_body>[^\r\n]*)[\r\n]*$',re.MULTILINE)
    regx_parm=re.compile(r'[:,]')
    parser = optparse.OptionParser(usage="%prog")
    parser.set_defaults(loglevel=logging.INFO)
    parser.add_option("-d", "--debug",  action="store_const", const=logging.DEBUG, dest="loglevel")
    parser.add_option("-i", "--interval",  default=10, dest="my_interval")
    parser.add_option("-r", "--rec", action="store_true", default=False)
    parser.add_option("-k", "--subkey", type='int', default=2)
    parser.add_option("-t", "--targetkey", type='int', default=3)

    (options,args) = parser.parse_args()
    logging.basicConfig(level=options.loglevel, format="%(asctime)s %(name)s %(levelname)s %(message)s" )
    my_interval=int(options.my_interval)
    live_logging=not options.rec
    summary_data=Summary()

    oldts=0
    columns=[]
    count={}
    argc = len(args)

    logging.debug("args=%s,argc=%s",args,argc)

    if argc > 1:
        logging.error("Too many arguments")
        exit()

    data={}
    data_idx=[]
    data_col=[]

    my_input=open_file(*args)
    for raw_line in iter(my_input.readline,""):
        r1 = regx_mxlog.search(raw_line)
        if r1:
          if r1.group('mx_event') == 'MsgTrace(65/26)':
            r2 = regx_parm.split(r1.group('mx_body'))
            alternate_event="{0}:{1}".format(r1.group('mx_event'),"_".join(r2[0].split(' ')[0:3]))
            print '{0}\t{1}\t{2}\t{3}\t{4}'.format(r1.group('time'),r1.group('mx_host'),r1.group('mx_module'),r1.group('mx_level'),alternate_event)
            sys.stdout.flush()
          else:
            print '{0}\t{1}\t{2}\t{3}\t{4}'.format(r1.group('time'),r1.group('mx_host'),r1.group('mx_module'),r1.group('mx_level'),r1.group('mx_event'))
            sys.stdout.flush()



if __name__ == '__main__':
    main()

