#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob
import pwd
import grp
import time
import datetime
import sched
import re
import json

regx_cmd1 = re.compile( r'^(?P<cmd>\([^\)]+\))' )
regx_cmd2 = re.compile( r'(^/([^\s/]+/){0,}){0,}(?P<cmd>[^/\s]+)' )
hz = os.sysconf(os.sysconf_names['SC_CLK_TCK'])

colStat=['pid', 'comm', 'state', 'ppid', 'pgrp', 'session', 'tty_nr', 'tpgid', 'flags', 'minflt', 'cminflt', 'majflt', 'cmajflt', 'utime', 'stime', 'cutime', 'cstime', 'priority', 'nice', 'num_threads', 'itrealvalue', 'starttime', 'vsize', 'rss', 'rsslim']

colStat.append('delta')
colStat.append('cmdline')

colToKeepStat=['pid','utime', 'stime', 'cutime', 'cstime', 'starttime', 'delta', 'num_threads', 'cmdline']
colToKeepStatus=['Uid', 'Gid', 'VmSize', 'VmRSS', 'VmPeak', 'VmHWM', 'VmStk', 'VmData','VmExe','VmSwap', 'voluntary_ctxt_switches', 'nonvoluntary_ctxt_switches']

def writeOneLine(filename,contents):
    try:
        my_file = open(filename,'w')
        my_file.write(contents)
        my_file.close()
    except Exception, e:
        sys.exit("### Unexpected error at {0}: {1} {2}\n".format(__name__,type(e),e.args))
    finally:
        try:
            if not my_file.closed :
                my_file.close()
        except:
            pass
    return 0


def readOneLine(filename):
    my_line=""
    try:
        my_file = open(filename)
        my_line = my_file.readline().rstrip()
    except IOError, (errno, sterror):
        sys.stdout.write("I/O error({0}): {1}".format(errno, stderror))
        return ""
    except Exception, e:
        sys.exit("### Unexpected error at {0}: {1} {2}\n".format(__name__,type(e),e.args))
    finally:
        try:
            if not my_file.closed :
                my_file.close()
        except:
            pass
    return my_line

def readLinesToDict(filename,key_value_regx= re.compile( r"^([^:]+)\s*:\s*(.*)$" ) ):
    my_line=""
    my_dict={}
    try:
        my_file = open(filename)
        for my_line in my_file:
            regx_result = key_value_regx.search(my_line)
            if regx_result != None:
                my_dict[regx_result.group(1)]=regx_result.group(2)
    except IOError, (errno, sterror):
        sys.stdout.write("I/O error({0}): {1}".format(errno, stderror))
        return ""
    except Exception, e:
        sys.exit("### Unexpected error at {0}: {1} {2}\n".format(__name__,type(e),e.args))
    finally:
        try:
            if not my_file.closed :
                my_file.close()
        except:
            pass
    return my_dict


def getPidData():
    pid_data_now = {}
    procDir = '/proc'
    cpustatFile = 'stat'
 
    os.chdir( procDir )
    pidList = glob.glob('[0-9]*')

    for pid in pidList:
        statFile = pid+'/stat'
        cmdlineFile = pid+'/cmdline'
        statusFile = pid+'/status'

        rawCpustat = readOneLine(cpustatFile)
        rawStat = readOneLine(statFile)
        rawCmdline = readOneLine(cmdlineFile)
        dictStatus = readLinesToDict(statusFile)

        if len(rawStat)>1 :
            listStat=rawStat.split()[0:len(colStat)-2] # 2 shows delta and cmdline
            if len(rawCmdline) <1:
                rawCmdline = listStat[1]
            list_tmp_cpustat=[int(s) for s in rawCpustat.split()[1:]]
            listStat.append(sum(list_tmp_cpustat))
            listStat.append(rawCmdline.replace('\0',' '))
            
            pid_data_now[pid]={}
            for item in colToKeepStat:
                pid_data_now[pid][item]=listStat[colStat.index(item)]
            for item in colToKeepStatus:
                try:
                    pid_data_now[pid][item]=dictStatus[item]
                except KeyError:
                    pid_data_now[pid][item]="null"
                except Exception, e:
                    sys.exit("### Unexpected error at {0}: {1} {2}\n".format(__name__,type(e),e.args))
    return pid_data_now

def getCpuUsage(dictOld,dictNow):
    olduRaw=int(dictOld['utime'])+int(dictOld['cutime'])
    nowuRaw=int(dictNow['utime'])+int(dictNow['cutime'])
    oldsRaw=int(dictOld['stime'])+int(dictOld['cstime'])
    nowsRaw=int(dictNow['stime'])+int(dictNow['cstime'])
    uRaw=float(nowuRaw-olduRaw)
    sRaw=float(nowsRaw-oldsRaw)
    totalRaw=float(uRaw+sRaw)
    denomi = float(dictNow['delta'])-float(dictOld['delta'])
    return (100*totalRaw/denomi,100*uRaw/denomi,100*sRaw/denomi)
    

def getCpuUsageFirst(dictNow):
    nowuRaw=int(dictNow['utime'])+int(dictNow['cutime'])
    nowsRaw=int(dictNow['stime'])+int(dictNow['cstime'])
    uRaw=float(nowuRaw)
    sRaw=float(nowsRaw)
    totalRaw=float(uRaw+sRaw)
    denomi = float(dictNow['delta'])-float(dictNow['starttime'])
    return (100*totalRaw/denomi,100*uRaw/denomi,100*sRaw/denomi)


def procInfoCreated(orig_dict):
    to_report_dict = {}
    to_report_dict['pid'] = int(orig_dict['pid'])
    to_report_dict['num_threads'] = int(orig_dict['num_threads'])
    if orig_dict.has_key('vountary_ctxt_switches') and orig_dict.has_key('nonvountary_ctxt_switches') :
        to_report_dict['v_cs'] = int(orig_dict['voluntary_ctxt_switches'])
        to_report_dict['nv_cs'] = int(orig_dict['nonvoluntary_ctxt_switches'])
    else:
        to_report_dict['v_cs'] = 0
        to_report_dict['nv_cs'] = 0
    to_report_dict['cmdline'] = orig_dict['cmdline']

    (to_report_dict['CPU'],to_report_dict['usr'],to_report_dict['sys'])=getCpuUsageFirst(orig_dict)

    regx=re.compile(r"(\d+)\skB")
    for key in ['VmSize', 'VmRSS', 'VmPeak', 'VmHWM', 'VmStk', 'VmData','VmExe','VmSwap' ]:
        result_regx = regx.search(orig_dict[key])
        if result_regx != None:
            to_report_dict[key] = int(result_regx.group(1))
        else:
            to_report_dict[key] = 0

    to_report_dict['Uid'] = pwd.getpwuid(int(orig_dict['Uid'].split()[0])).pw_name
    to_report_dict['Gid'] = grp.getgrgid(int(orig_dict['Gid'].split()[0])).gr_name


    return to_report_dict

def procInfoContinue(orig_dict_old,orig_dict_now):
    to_report_dict = {}

    to_report_dict['pid'] = int(orig_dict_now['pid'])
    to_report_dict['num_threads'] = int(orig_dict_now['num_threads'])
    to_report_dict['cmdline'] = orig_dict_now['cmdline']
    if orig_dict_now.has_key('vountary_ctxt_switches') and orig_dict_now.has_key('nonvountary_ctxt_switches') :
        to_report_dict['v_cs'] = int(orig_dict_now['voluntary_ctxt_switches'])
        to_report_dict['nv_cs'] = int(orig_dict_now['nonvoluntary_ctxt_switches'])
    else:
        to_report_dict['v_cs'] = 0
        to_report_dict['nv_cs'] = 0

    (to_report_dict['CPU'],to_report_dict['usr'],to_report_dict['sys'])=getCpuUsage(orig_dict_old,orig_dict_now)

    regx=re.compile(r"(\d+)\skB")
    for key in ['VmSize', 'VmRSS', 'VmPeak', 'VmHWM', 'VmStk', 'VmData','VmExe','VmSwap' ]:
        result_regx = regx.search(orig_dict_now[key])
        if result_regx != None:
            to_report_dict[key] = int(result_regx.group(1))
        else:
            to_report_dict[key] = 0
    to_report_dict['Uid'] = pwd.getpwuid(int(orig_dict_now['Uid'].split()[0])).pw_name
    to_report_dict['Gid'] = grp.getgrgid(int(orig_dict_now['Gid'].split()[0])).gr_name

    return to_report_dict


def procInfoDeleted(orig_dict):
    to_report_dict = {}
    to_report_dict['pid'] = int(orig_dict['pid'])
    to_report_dict['num_threads'] = 0
    to_report_dict['v_cs'] = 0
    to_report_dict['nv_cs'] = 0
    to_report_dict['cmdline'] = orig_dict['cmdline']
    (to_report_dict['CPU'],to_report_dict['usr'],to_report_dict['sys'])=(0.0,0.0,0.0) 

    regx=re.compile(r"(\d+)\skB")
    for key in ['VmSize', 'VmRSS', 'VmPeak', 'VmHWM', 'VmStk', 'VmData','VmExe','VmSwap' ]:
        to_report_dict[key] = 0

    to_report_dict['Uid'] = pwd.getpwuid(int(orig_dict['Uid'].split()[0])).pw_name
    to_report_dict['Gid'] = grp.getgrgid(int(orig_dict['Gid'].split()[0])).gr_name

    return to_report_dict


def outputProcInfo(datetime_string,doa_char,d):
    #sys.stdout.write("%s %c %6.2f %5.2f %5.2f %5d %8s %7d %7d %7d %7d %5d %7d %7d %7d %6d %6d %6d %s"% (datetime_string,doa_char,d['CPU'],d['usr'],d['sys'],d['pid'],d['Uid'],d['VmSize'], d['VmPeak'],d['VmRSS'], d['VmHWM'],d['VmStk'], d['VmData'],d['VmExe'],d['VmSwap'],d['nv_cs'],d['v_cs'],d['num_threads'],d['cmdline'])
    d['time']=datetime_string
    #d['label']="{0}({1})".format(' '.join(d['cmdline'].split(' ')[0:3]),d['pid'])
    regx_result = regx_cmd1.search(d['cmdline'])
    if not regx_result:
      regx_result = regx_cmd2.search(d['cmdline'])
    if regx_result:
      tmp_cmd=regx_result.group('cmd')
    else:
      tmp_cmd=d['cmdline']
    d['label']=tmp_cmd
    d['CPU']=round(d['CPU'],3)
    d['usr']=round(d['usr'],3)
    d['sys']=round(d['sys'],3)
    sys.stdout.write(json.dumps(d))
    '''
    regx_result = regx_cmd1.search(d['cmdline'])
    if regx_result:
      sys.stdout.write(regx_result.group('cmd'))
    else:
      regx_result = regx_cmd2.search(d['cmdline'])
      if regx_result:
        if regx_result.group('cmd') in ['java','perl','python','sh','bash','csh','ruby']:
          if regx_result.group('cmd') in ['java']:
            #sys.stdout.write("{0} {1}".format(regx_result.group('cmd'),d['cmdline'].split(' ')[-1]))
            sys.stdout.write(regx_result.group('cmd'))
          else:
            #sys.stdout.write("{0} {1}".format(regx_result.group('cmd'),d['cmdline'].split(' ')[2:3]))
            sys.stdout.write(regx_result.group('cmd'))
        else:
          sys.stdout.write(regx_result.group('cmd'))
    '''
    sys.stdout.write("\n")


def dummy_func():
    pass



if "__main__" == __name__:
    import optparse

    parser = optparse.OptionParser()
    (options,args) = parser.parse_args()

    interval = int(args[0])

    num_of_cpu=len(readLinesToDict('/proc/stat',re.compile(r"^(cpu\d) (.*)$")))


    pid_data_now={}
    pid_data_old={}

    schedule = sched.scheduler(time.time, time.sleep)

    while True:
        timestamp_now = time.time()
        pid_data_now = getPidData()
        pid_set_now = set(pid_data_now.keys())
        pid_set_old = set(pid_data_old.keys())
        pid_set_created  =  pid_set_now - pid_set_old
        pid_set_deleted  =  pid_set_old - pid_set_now
        pid_set_continue =  pid_set_now & pid_set_old

        #tm_string= str(datetime.datetime.fromtimestamp(timestamp_now))[0:19] # YYYY-mm-dd HH:MM:SS
        tm_string= int(timestamp_now)

        #sys.stdout.write("#                 doa   CPU  usr  sys   pid    uname     vsz   vpeak     rsz   rpeak stack    data    text    swap  nv_cs   v_cs   nlwp cmdline")


        for i_pid in pid_set_created:
            report_dict=procInfoCreated(pid_data_now[i_pid])
            outputProcInfo(tm_string,'+',report_dict)


        pid_list_continue_show = []
        for i_pid in pid_set_continue:
#            for chk_key in ['VmSize', 'VmRSS','utime', 'stime', 'cutime', 'cstime']:
#                if pid_data_now[i_pid][chk_key] != pid_data_old[i_pid][chk_key]:
#                    pid_list_continue_show.append(i_pid)
#                    break
#        for i_pid in pid_list_continue_show:
            report_dict=procInfoContinue(pid_data_old[i_pid],pid_data_now[i_pid])
            outputProcInfo(tm_string,'c',report_dict)

        for i_pid in pid_set_deleted:
            report_dict=procInfoDeleted(pid_data_old[i_pid])
            outputProcInfo(tm_string,'-',report_dict)

                 
        sys.stdout.flush()

        pid_data_old = pid_data_now
        schedule.enter(timestamp_now+interval - time.time(), 1, dummy_func,())
        schedule.run()


