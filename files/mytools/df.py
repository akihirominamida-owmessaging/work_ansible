#! /usr/bin/env python
# -*- coding: utf-8 -*-

import time
import datetime
import sched
import sys
import os

import optparse
import commands

def dummy_func():
    pass

def writeOneLine(filename,contents):
    try:
        my_file = open(filename,'w')
        my_file.write(contents)
        my_file.close()
    except Exception, e:
        print "### exception %s ###"%__name__
        print str(type(e))
        print str(e.args)
        return -1
    finally:
        try:
            if not my_file.closed :
                my_file.close()
        except:
            pass
    return 0


parser = optparse.OptionParser()
parser.add_option('-p','--pid', action="store")
(options,args) = parser.parse_args()
(interval, sampling_count) = [int(s) for s in args]

parser = optparse.OptionParser()
parser.add_option('-p','--pid', action="store")
(options,args) = parser.parse_args()
if options.pid != None:
    os.chdir(options.pid)
else:
    pass
if writeOneLine(os.path.basename(sys.argv[0])+".pid",str(os.getpid())+"\n") != 0:
    sys.exit(-1)



schedule = sched.scheduler(time.time, time.sleep)

for i_sampling in range(sampling_count):
    timestamp_now = time.time()
    tm_string = str(datetime.datetime.fromtimestamp(timestamp_now))[0:19].replace(' ','/')

    rawData = commands.getoutput('LC_ALL=C df -kPT')
    rawData = rawData.splitlines()

    for i in rawData:
        i = i.replace('Mounted on','Mounted-on')
        print "%s %s"% (tm_string,i)
        sys.stdout.flush()

    if i_sampling < sampling_count - 1:
        schedule.enter(timestamp_now+interval - time.time(), 1, dummy_func,())
        schedule.run()
