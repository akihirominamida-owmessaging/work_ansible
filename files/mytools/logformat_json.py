#!/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import datetime
import time
import logging
import optparse

def open_file(file=None, flag="r"):
    logging.debug('[open_file]')
    if file is not None :
        try:
            return open(file,flag)
        except:
            logging.warning("Fail to open file %s. Openning sys.stdin instead of.", file)
    if flag == "r":
        return sys.stdin
    elif flag == "rw":
        return sys.stdout


def main():

    regx_mxlog=re.compile(r'^(\d{4}\d{2}\d{2} \d{2}\d{2}\d\d)\d{3}[\+\-]\d{4} (\S+) (?P<name_module>\S+) (\d+) (\d+) (\d+) ([^;]+);([^: \r\n]+)([^\r\n]*)[\r\n]*$',re.MULTILINE)
    regx_parm=re.compile(r'[:,]')
    regx_each_parm=re.compile(r'[=]')

#    treat_as_number_list=["size","len","msgsize","mtd_defercount","mtd_deliveryattempts","mtd_deliverytime","mtd_totaltime","time"]
#    tread_as_number={}
#    for key in treat_as_number_list:
#      tread_as_number[key]=True
 
    parser = optparse.OptionParser(usage="%prog")
    parser.set_defaults(loglevel=logging.INFO)
    parser.add_option("-d", "--debug",  action="store_const", const=logging.DEBUG, dest="loglevel")
    parser.add_option("-l", "--list", type="string")

    (options,args) = parser.parse_args()
    logging.basicConfig(level=options.loglevel, format="%(asctime)s %(name)s %(levelname)s %(message)s" )
    if options.list is not None:
        parms_to_show=options.list.split(',')
    else:
        parms_to_show = ""

    oldts=0
    columns=[]
    count={}
    argc = len(args)

    logging.debug("args=%s,argc=%s",args,argc)

    if argc > 1:
        logging.error("Too many arguments")
        exit()

    my_input=open_file(*args)
    for raw_line in my_input:
        regx_result=regx_mxlog.search(raw_line)
        if regx_result is not None:
           currentTime = int(time.mktime(time.strptime(regx_result.group(1), "%Y%m%d %H%M%S")))
           (cHost,cServer,cPid,cTid)=regx_result.group(2,3,4,6)
           (cLevel,cEvent,cParam)=regx_result.group(7,8,9)
           cParam_list = [x.strip() for x in regx_parm.split(cParam)]
           cParam_dict = {}
           for entity in cParam_list:
               each_parm=regx_each_parm.split(entity) 
               if len(each_parm) == 2:
                   cParam_dict[each_parm[0]]=each_parm[1]
           if cEvent.startswith("MsgTrace"):
               cEvent_name = "MsgTrace {0}".format(cParam_list[0])
           else:
               cEvent_name = cEvent.split("(")[0]
           sys.stdout.write('{{"time":{0},"host":"{1}","module":"{2}","pid":"{3}","tid":"{4}","level":"{5}","event":"{6}"'.format(currentTime,cHost,cServer,cPid,cTid,cLevel,cEvent_name))
           #for parm in parms_to_show:
           #   if cParam_dict.get(parm) is None:
           #       #sys.stdout.write("{0:16s}".format("None"))
           #       pass
           #   else:
           #       sys.stdout.write(',"{0}":"{1}"'.format(parm,cParam_dict[parm]))
           for parm in cParam_dict.keys():
#             if tread_as_number.get(parm):
#               sys.stdout.write(',"{0}":{1}'.format(parm,cParam_dict[parm].strip()))
#             else:
#               sys.stdout.write(',"{0}":"{1}"'.format(parm,cParam_dict[parm]))
             if parm in ["time","host","module","pid","tid","level","event"]:
               sys.stdout.write(',"param_{0}":"{1}"'.format(parm,cParam_dict[parm]))
             else:
               sys.stdout.write(',"{0}":"{1}"'.format(parm,cParam_dict[parm]))
           sys.stdout.write("}\n")
           sys.stdout.flush()
    my_input.close()


if __name__ == '__main__':
    main()


