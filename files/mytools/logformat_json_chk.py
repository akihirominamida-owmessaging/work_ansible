#!/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import datetime
import time
import logging
import optparse

def open_file(file=None, flag="r"):
    logging.debug('[open_file]')
    if file is not None :
        try:
            return open(file,flag)
        except:
            logging.warning("Fail to open file %s. Openning sys.stdin instead of.", file)
    if flag == "r":
        return sys.stdin
    elif flag == "rw":
        return sys.stdout

def main():

    regx_mxlog=re.compile(r'^(\d{4})(\d{2})(\d{2}) (\d{2})(\d{2})(\d)(\d)\d{3}[+-]\d{4} (\S+) (?P<name_module>\S+) (\d+) (\d+) (\d+) ([^;]+);([^: \r\n]+)([^\r\n]*)[\r\n]*$',re.MULTILINE)
    regx_parm=re.compile(r'[:,]')
    regx_each_parm=re.compile(r'[=]')
    parser = optparse.OptionParser(usage="%prog")
    parser.set_defaults(loglevel=logging.INFO)
    parser.add_option("-d", "--debug",  action="store_const", const=logging.DEBUG, dest="loglevel")
    parser.add_option("-i", "--interval",  default=10, dest="my_interval")
    parser.add_option("-r", "--rec", action="store_true", default=False)
    parser.add_option("-l", "--list", type="string")

    (options,args) = parser.parse_args()
    logging.basicConfig(level=options.loglevel, format="%(asctime)s %(name)s %(levelname)s %(message)s" )
    my_interval=int(options.my_interval)
    live_logging=not options.rec
    if options.list is not None:
        parms_to_show=options.list.split(',')
    else:
        parms_to_show = ""

    oldts=0
    columns=[]
    count={}
    argc = len(args)

    logging.debug("args=%s,argc=%s",args,argc)

    if argc > 1:
        logging.error("Too many arguments")
        exit()

    my_input=open_file(*args)
    for raw_line in my_input:
        regx_result=regx_mxlog.search(raw_line)
        if regx_result is not None:
           currentTime = "%s/%s/%s-%s:%s:%s%s" % regx_result.group(1,2,3,4,5,6,7)
           (cHost,cServer,cPid,cTid)=regx_result.group(8,9,10,12)
           (cLevel,cEvent,cParam)=regx_result.group(13,14,15)
           cParam_list = [x.strip() for x in regx_parm.split(cParam)]
           cParam_dict = {}
           for entity in cParam_list:
               each_parm=regx_each_parm.split(entity) 
               if len(each_parm) == 2:
                   cParam_dict[each_parm[0]]=each_parm[1]
           if cEvent.startswith("MsgTrace"):
               cEvent_name = "MsgTrace {0}".format(cParam_list[0])
           else:
               cEvent_name = cEvent.split("(")[0]
           #sys.stdout.write('{{"time":"{0}","host":"{1}","module":"{2}","pid":"{3}","tid":"{4}","level":"{5}","event":"{6}"'.format(currentTime,cHost,cServer,cPid,cTid,cLevel,cEvent_name))
           for parmkey in cParam_dict.keys():
             if cParam_dict.get(parmkey):
               #sys.stdout.write("{0}:{1}\n".format(parmkey,cParam_dict[parmkey]))
               sys.stdout.write("{0}:\n".format(parmkey))
             else:
               sys.stdout.write("{0}:\n".format(parmkey))
           for parm in parms_to_show:
              if cParam_dict.get(parm) is None:
                  #sys.stdout.write("{0:16s}".format("None"))
                  pass
              else:
                  #sys.stdout.write(',"{0}":"{1}"'.format(parm,cParam_dict[parm]))
                  pass
           #sys.stdout.write("}\n")
           sys.stdout.flush()
    my_input.close()


if __name__ == '__main__':
    main()


