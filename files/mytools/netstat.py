#! /usr/bin/env python
# -*- coding: utf-8 -*-

import time
import datetime
import sched
import os
import sys

import optparse
import commands

def dummy_func():
    pass

def writeOneLine(filename,contents):
    try:
        my_file = open(filename,'w')
        my_file.write(contents)
        my_file.close()
    except Exception, e:
        print "### exception %s ###"%__name__
        print str(type(e))
        print str(e.args)
        return -1
    finally:
        try:
            if not my_file.closed :
                my_file.close()
        except:
            pass
    return 0


parser = optparse.OptionParser()
parser.add_option('-p','--pid', action="store")
(options,args) = parser.parse_args()
(interval, sampling_count) = [int(s) for s in args]

if options.pid != None:
    os.chdir(options.pid)
else:
    pass
if writeOneLine(os.path.basename(sys.argv[0])+".pid",str(os.getpid())+"\n") != 0:
    sys.exit(-1)



schedule = sched.scheduler(time.time, time.sleep)

for i_sampling in range(sampling_count):
    timestamp_now = time.time()
    tm_string = str(datetime.datetime.fromtimestamp(timestamp_now))[0:19].replace(' ','/')

    rawData = commands.getoutput('LC_ALL=C netstat -onapet')
    rawData = rawData.splitlines()

    for i in rawData:
        if not i.startswith('Active Internet connections'):
            i = i.replace('Local Address','Local-Address')
            i = i.replace('Foreign Address','Foreign-Address')
            print "%s %s"% (tm_string,i)

    if i_sampling < sampling_count - 1:
        schedule.enter(timestamp_now+interval - time.time(), 1, dummy_func,())
        schedule.run()
