import time
import sys
import pexpect

def ringsh_config(my_url="http://localhost:3080", my_ringname="ring1", my_scalitykey="scalitykey", my_brs2key="brs2key"):

    rc = pexpect.spawn("/usr/local/bin/ringsh-config")
    rc.logfile = sys.stdout
    rc.expect("\(dsup port\): \[https://localhost:3443\] ",timeout=3000)
    rc.sendline(my_url)
    rc.expect("the name of the ring: ")
    rc.sendline(my_ringname)
    rc.expect("skip this phase: ")
    rc.sendline(my_scalitykey)
    rc.expect("BRS2 secret key: ")
    rc.sendline(my_brs2key)
    rc.expect(pexpect.EOF)

def ringsh1(my_ringname="ring1"):

    rc = pexpect.spawn("/usr/local/bin/ringsh.py")
    rc.logfile = sys.stdout
    rc.expect("ring> ",timeout=3000)
    rc.sendline("supervisor dsoCreate {0}".format(my_ringname))
    rc.expect("ring> ",timeout=3000)
    rc.sendline("exit")
    rc.expect(pexpect.EOF)

def ringsh2(my_ips,my_url="http://localhost:3080", my_ringname="ring1", my_scalitykey="scalitykey", my_brs2key="brs2key"):

    rc = pexpect.spawn("/usr/local/bin/ringsh.py")
    rc.logfile = sys.stdout
    rc.expect("ring> ",timeout=3000)
    rc.sendline("supervisor loadConf {0}".format(my_ringname))
    for host in my_ips:
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverAdd conn{0}1 {1} 8184 1".format(my_ips.index(host),host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverAdd node{0}1 {1} 8084 1".format(my_ips.index(host),host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverAdd node{0}2 {1} 8085 1".format(my_ips.index(host),host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverAdd node{0}3 {1} 8086 1".format(my_ips.index(host),host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverAdd node{0}4 {1} 8087 1".format(my_ips.index(host),host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverAdd node{0}5 {1} 8088 1".format(my_ips.index(host),host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverAdd node{0}6 {1} 8089 1".format(my_ips.index(host),host))
    time.sleep(3)
    there_are_offline_node = True 
    while there_are_offline_node:
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor serverList")
      rc.expect("ring> ",timeout=3000)
      there_are_offline_node = ("offline" in rc.before)
      rc.sendline("")

    for host in my_ips:
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeSetDso {0} {1} 8084".format(my_ringname,host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeSetDso {0} {1} 8085".format(my_ringname,host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeSetDso {0} {1} 8086".format(my_ringname,host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeSetDso {0} {1} 8087".format(my_ringname,host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeSetDso {0} {1} 8088".format(my_ringname,host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeSetDso {0} {1} 8089".format(my_ringname,host))
      time.sleep(3)

    for host in my_ips:
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeJoin {0} 8084".format(host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeJoin {0} 8085".format(host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeJoin {0} 8086".format(host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeJoin {0} 8087".format(host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeJoin {0} 8088".format(host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor nodeJoin {0} 8089".format(host))
      time.sleep(3)

    rc.expect("ring> ",timeout=3000)
    rc.sendline("supervisor loadConf {0}".format(my_ringname))

    bootstrap_arg=""
    delimit=""
    for host in my_ips:
      bootstrap_arg+="{0}{1}:4244".format(delimit,host)
      delimit=","
      
    for host in my_ips:
      rc.expect("ring> ",timeout=3000)
      rc.sendline("supervisor connectorSetDso {0} {1}:8184".format(my_ringname,host))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("use conn{0}1".format(my_ips.index(host)))
      rc.expect("ring> ",timeout=3000)
      rc.sendline("accessor configSet msgstore_storage_chordapi bootstrapnode {0}".format(bootstrap_arg))

    rc.sendline("exit")
    rc.expect(pexpect.EOF)

def get_node_ip_list(hostsfile='/etc/hosts',bloburl='blobcluster.com'):
    ip_list=[]
    with open(hostsfile) as f:
      for line in f:
        if bloburl in line:
          ip_list.append(line.split(' ')[0])
    return ip_list

def main():
    ips=get_node_ip_list()
    ringsh_config(my_url="http://localhost:3080", my_ringname="ring1")
    ringsh1(my_ringname="ring1")
    ringsh_config(my_url="http://localhost:3080", my_ringname="ring1")
    ringsh2(my_ips=ips,my_ringname="ring1")
    ringsh_config(my_url="http://localhost:3080", my_ringname="ring1")

if __name__ == '__main__':
    main()


