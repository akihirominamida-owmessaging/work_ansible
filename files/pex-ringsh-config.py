import time
import sys
import pexpect

def ringsh_config(my_url="", my_ringname="my_ring", my_scalitykey="scalitykey", my_brs2key="brs2key"):

    rc = pexpect.spawn("/usr/local/bin/ringsh-config")
    rc.logfile = sys.stdout
    rc.expect("\(dsup port\): \[https://localhost:3443\] ",timeout=3000)
    rc.sendline(my_url)
    rc.expect("the name of the ring: ")
    rc.sendline(my_ringname)
    rc.expect("skip this phase: ")
    rc.sendline(my_scalitykey)
    rc.expect("BRS2 secret key: ")
    rc.sendline(my_brs2key)
    rc.expect(pexpect.EOF)

def main():
    ringsh_config(my_url="http://localhost:3080",my_ringname="ring1")

if __name__ == '__main__':
    main()


